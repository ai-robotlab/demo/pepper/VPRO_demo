<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Pepper Demo vpro" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="" src=".DS_Store" />
        <File name="translation_en_US" src="translations/translation_en_US.ts" />
        <File name="choice_sentences" src="behavior_1/Aldebaran/choice_sentences.xml" />
    </Resources>
    <Topics />
    <IgnoredPaths />
</Package>
